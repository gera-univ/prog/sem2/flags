﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Flags
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DrawConfederateFlag();
        }

        public void DrawConfederateFlag()
        {
            var bgRect = new Rectangle { Width = 2, Height = 1, Fill = Brushes.White };
            var redRect = new Rectangle { Width = 0.5, Height = 1, Fill = Brushes.Firebrick };
            Canvas.SetLeft(redRect, 1.5);


            var banner = GetConfederateFlagBanner();
            banner.RenderTransform = new ScaleTransform(0.9,0.6,0,0);

            ConfederateFlagCanvas.Children.Add(bgRect);
            ConfederateFlagCanvas.Children.Add(redRect);
            ConfederateFlagCanvas.Children.Add(banner);
        }

        public Viewbox GetConfederateFlagBanner()
        {
            var banner = new Viewbox();
            var bannerCanvas = new Canvas {Width = 1, Height = 1, Background = Brushes.Firebrick};
            banner.Child = bannerCanvas;

            // Рисуем синие полоски
            var topToBottomStripe = new Polygon
            {
                Points =
                {
                    new Point(0, 0.1), new Point(0, 0), new Point(0.1, 0), // верхний левый угол
                    new Point(1, 0.9), new Point(1, 1), new Point(0.9, 1) // нижний праый угол
                },
                Fill = Brushes.DarkBlue
            };
            var bottomToTopStripe = new Polygon
            {
                Points = topToBottomStripe.Points,
                RenderTransform = new RotateTransform {CenterX = 0.5, CenterY = 0.5, Angle = 90}, // поворот на 90
                Fill = Brushes.DarkBlue
            };
            bannerCanvas.Children.Add(topToBottomStripe);
            bannerCanvas.Children.Add(bottomToTopStripe);

            // Рисуем белые полоски
            var whiteStripePoints = new PointCollection
            {
                new Point(0.1, 0), new Point(0.13, 0), new Point(0.5, 0.37), new Point(0.5, 0.4),
            };
            for (int i = 1; i <= 4; i++)
            {
                bannerCanvas.Children.Add(new Polygon
                {
                    Points = whiteStripePoints,
                    Fill = Brushes.White,
                    //Stroke = Brushes.White,
                    //StrokeThickness = 0.001,
                    RenderTransform = new TransformGroup
                    {
                        Children =
                        {
                            new RotateTransform
                            {
                                CenterX = 0.5,
                                CenterY = 0.5,
                                Angle = 90 * i
                            }
                        }
                    }
                });
                bannerCanvas.Children.Add(new Polygon
                {
                    Points = whiteStripePoints,
                    Fill = Brushes.White,
                    //Stroke = Brushes.White,
                    //StrokeThickness = 0.001,
                    RenderTransform = new TransformGroup
                    {
                        Children =
                        {
                            new ScaleTransform {ScaleX = -1, CenterX = 0.5},
                            new RotateTransform
                            {
                                CenterX = 0.5,
                                CenterY = 0.5,
                                Angle = 90 * i
                            }
                        }
                    }
                });
            }

            //bannerCanvas.Children.Add(whiteStripe);
            //bannerCanvas.Children.Add(whiteStripeMirrored);

            for (int i = 0; i < 3; i++)
            {
                double coord = 0.1 + 0.12*i;
                var star = GetConfederateFlagStar(coord, coord, 0.1);
                bannerCanvas.Children.Add(star);
                bannerCanvas.Children.Add(FlipPolygon(star, 0.5, coord, true, true));
                bannerCanvas.Children.Add(FlipPolygon(star, 0.5, 0.5, true, true));
                bannerCanvas.Children.Add(FlipPolygon(star, 0.5, 0.5, false, true));
            }


            bannerCanvas.Children.Add(GetConfederateFlagStar(0.5, 0.5, 0.1));
            return banner;
        }

        public Polygon FlipPolygon(Polygon p, double centerX, double centerY, bool flipX, bool flipY)
        {
            var result = new Polygon
            {
                Points = p.Points,
                Fill = p.Fill
            };


            result.RenderTransform = new TransformGroup
            {
                Children =
                {
                    result.RenderTransform,
                    new ScaleTransform
                    {
                        CenterX = centerX,
                        CenterY = centerY,
                        ScaleX = flipX ? -1 : 1,
                        ScaleY = flipY ? -1 : 1
                    }
                }
            };
            return result;
        }

        public Polygon GetConfederateFlagStar(double centerX, double centerY, double scale)
        {
            return new Polygon
            {
                Points =
                {
                    new Point(centerX + scale * 0, centerY + scale * 0.5),
                    new Point(centerX + scale * 0.1, centerY + scale * 0.12),
                    new Point(centerX + scale * 0.45, centerY + scale * 0.12),
                    new Point(centerX + scale * 0.18, centerY + scale * -0.12),
                    new Point(centerX + scale * 0.285, centerY + scale * -0.5),
                    new Point(centerX + scale * 0, centerY + scale * -0.26),
                    new Point(centerX + scale * -0.285, centerY + scale * -0.5),
                    new Point(centerX + scale * -0.18, centerY + scale * -0.12),
                    new Point(centerX + scale * -0.45, centerY + scale * 0.12),
                    new Point(centerX + scale * -0.1, centerY + scale * 0.12)
                },
                Fill = Brushes.White,
                RenderTransform = new ScaleTransform {CenterY = centerY, ScaleY = -1} // отражаем звезду остриём вниз
            };
        }
    }
}